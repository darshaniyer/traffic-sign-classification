

# Traffic sign recognition

Recognizing different traffic signs with high accuracy is a must-have requirement for any self driving car. The traffic sign recognition is a classification problem that could be solved with traditional computer vision approach involving building a pipeline consisting of representing different traffic signs with hand-crafted features and classifying them using traditional classifier like support vector machine (SVM), random forest (RF), etc. Generating hand-crafted features is a challenging task due to wide variability in the traffic sign data. Recently, the subfield of machine learning called deep learning have shown highly promising performance in wide variety of applications due to recent advancements in computational power and scientific methods. This has made possible the implementation of computationally demanding neural networks with deep architectures which have the property of universal function approximation. In other words, neural networks can be used to fit any nonlinear function with enough number of layers, each represented with lots of parameters. The parameters can later be pruned with regularization methods such as dropout, L2 regularization, etc. to enhance the generalization ability of the network architecture to new sets of images. 

In this study, we apply the deep learning architecture based on a combination of convolutional neural networks (CNNs) and fully connected layers for modeling traffic sign images. The data used are [German traffic sign](http://benchmark.ini.rub.de/?section=gtsrb&subsection=dataset) images and the base neural network architecture is [LeNet](http://yann.lecun.com/exdb/publis/pdf/lecun-01a.pdf). The processing steps include data exploration  and visualization for understanding the data, normalization for preprocessing the data, training the preprocessed data using deep learning architecture that includes convolutional, max pooling, fully connected layers interspersed with nonlinearity based on rectified linear units (ReLUs) and regularization based on dropout and L2 regularization to build the model. 
The impact of each of these processing steps is illustrated during the process of hyperparameter selection for parameters such as learning rate, dropout rate, weight regularization, batch size, and number of epochs. Adam optimizer is used for optimizing the network parameters. The analysis uses LeNet architecture as the basic architecture, and the impact of adding an additional convolutional layer is also presented. To make the model more robust to influence of varieties of environmental sources of noise such as abrupt weather conditions, changes in lightning conditions, etc. data augmentation methods are applied to the training data before building the final model. The performance is quantified using metrics based on overall accuracy, and recall, precision, and f1-score for each of the classes. The final model is also applied on an independent set of 5 images downloaded from the web.

## Data description

The German traffic sign dataset consists of RGB color images of 32x32 size, each belonging to one of the 43 classes. In total, there are 34799 training, 4410 validation, and 12630 test samples. Following are the distributions of these datasets.

[image1]: ./figures/Training_data_distribution.jpg "Training data distribution"
![alt text][image1]
**Figure 1: Training data distribution**

[image2]: ./figures/Validation_data_distribution.jpg "Validation data distribution"
![alt text][image2]
**Figure 2: Validation data distribution**

[image3]: ./figures/Test_data_distribution.jpg "Test data distribution"
![alt text][image3]
**Figure 3: Test data distribution**

As one can see the classes are imbalanced in terms of the number of samples in each of the classes. However, the distribution of data in the three sets is stratified to maintain the consistency of distribution. This helps in predicting the performance of the model on the independent test set (from the validation set performance), which is not used in either building the model as the training set or in model selection as the validation set.

The model is finally tested on an independent set of five images downloaded from the web.


## Preprocessing

Each image file consists of red, green, and blue channels with intensities in the range of 0 to 255. Each of the four sets (training, validation, test, and independent set) are normalized by dividing by 255 so that the intensities are in the range of 0 to 1. This is followed by subtracting 0.5 from the normalized values to ensure that the images are centered in their intensity values and that the model is fit to features of the sign and not the noise sources such as change in illumination due to weather, time of the day, and lightning conditions.

## Model architecture

[image4]: ./figures/lenet.jpg "LeNet architecture"
![alt text][image4]
**Figure 4: LeNet architecture**

### Input
The LeNet architecture accepts a 32x32xC image as input, where C is the number of color channels. 

### Architecture
**Layer 1: Convolutional.** A convolutional layer consisting of 6 5x5 convolutional filters, stride of 1, and no padding. The output shape is 28x28x6.

**Activation.** ReLU.

**Pooling.** A max pooling layer consisting of 2x2 pooling with a stride of 2. The output shape is 14x14x6. 

**Layer 2: Convolutional.** A convolutional layer consisting of 16 5x5 convolutional filters, stride of 1, and no padding. The output shape is 10x10x16.

**Activation.** ReLU.

**Pooling.** A max pooling layer consisting of 2x2 pooling with a stride of 2. The output shape is 5x5x16. 

**Flatten.** The output of the final pooling layer is flattened such that it is 1D instead of 3D. 

**Layer 3: Fully Connected.** The first fully connected layer has 120 outputs.

**Activation.** ReLU.

**Dropout.** Dropout layer

**Layer 4: Fully Connected.** The second fully connected layer has 84 outputs.

**Activation.** ReLU

**Dropout.** Dropout layer

**Layer 5: Fully Connected (Logits).** The final fully connected layer has 43 outputs equal to the number of distinct traffic signs.

### Output
The result of the last fully connected layer.

### Network parameters
Weights are initialized with normally distributed random values with a mean of 0 and sigma of 0.1; biases are initialized with zeros.

## Training and model evaluation

The training pipeline consists of forward and backward passes. The forward pass consists of calculating logits using the aforementioned architecture, converting logits into probabilities using softmax function, computing the cross entropy loss between the probabilites and the one hot encoded true labels, and finally computing the average loss. The backward pass consists of backpropagating the average loss through the network and optimizing the network parameters with an objective to improve the class prediction in the next epoch. Training the network is done for a stipulated number of epochs, where each epoch consists of passing every sample through the network once. Also, in each epoch the samples are broken down into mini batches of equal size (except for the last minibatch). The parameters are updated after every minibatch. After each epoch of training and parameter optimization, the performance is computed on the validation set to keep track of the progress.


## Impact of different processes on performance
This section illustrates the impact of normalization, number of epochs, minibatch size, learning rate, dropout, weight regularization, training data augmentation, and an additional layer in the model on an overall performance on the validation data. This step is used for visualization and selection of hyperparameter values for the final model.

### Impact of normalization
The number of epochs is 100, minibatch size is 128, no dropout, and learning rate is 0.001. Following figure depicts the impact of normalization on an overall performance which is quite significant.

[image5]: ./figures/Normalization.jpg "Impact of normalization"
![alt text][image5]
**Figure 5: Impact of normalization**

### Impact of number of epochs
The number of epochs is set to maximum of 100, minibatch size is 128, no dropout, and learning rate is 0.001. Following figure depicts the evolution of performance with the number of epochs. The performance seems to stabilize at 100 epochs.

[image6]: ./figures/NumEpochs.jpg "Impact of number of epochs"
![alt text][image6]
**Figure 6: Impact of number of epochs**

### Impact of minibatch size
The minibatch size is varied in 256, 128, 64, and 32 sample sizes, and for each run the number of epochs is set to maximum of 100, no dropout, and learning rate is 0.001. The minibatch size of 32 or 64 seems to give the best overall performance. Smaller minibatches tend to work better due to more frequent parameter updates.

[image7]: ./figures/Batchsize.jpg "Impact of minibatch size"
![alt text][image7]
**Figure 7: Impact of minibatch size**

### Impact of learning rate size
The learning rate is varied in 0.1, 0.01, 0.001, 0.0005, 0.0001, and for each run the number of epochs is set to maximum of 100, the minibatch size is set to 128, and no dropout. The learning rate of 0.001 or 0.0005 seems to give the best overall performance.

[image8]: ./figures/LearningRate.jpg "Impact of learning rate"
![alt text][image8]
**Figure 8: Impact of learning rate**

### Impact of dropout rate
The dropout rate is varied in 0 (no dropout), 0.25, 0.5, 0.75, and for each run the number of epochs is set to maximum of 100, the minibatch size is set to 128, and learning rate is set to 0.001. The dropout rate of 0.25 seems to give the best overall performance. The legend in the figure is keep rate which is 1.0-dropout rate.

[image9]: ./figures/DropOutRate.jpg "Impact of dropout rate"
![alt text][image9]
**Figure 9: Impact of dropout rate**

### Impact of weight regularization
The weights of fully connected layers are regularized using L2 regularization. The performance drops a little bit with regularization which might signal underfitting. The number of epochs are set to 100, minibatch size set to 128, no dropout, and learning rate set to 0.001.

[image10]: ./figures/WeightRegularization.jpg "Impact of weight regularization"
![alt text][image10]
**Figure 10: Impact of weight regularization**

### Impact of training data augmentation
The training data are augmented to improve the robustness of the model to variability in the data introduced by abrupt weather conditions (salt and pepper noise), non-uniform illumination (gaussian noise), and light of the day (randomizing color channels). The number of training samples increased from 34799 to 129196 samples after augmentation. The number of epochs are set to 100, minibatch size set to 128, no dropout, and learning rate set to 0.001. The performance on the validation set improves a little bit after augmentation.

[image11]: ./figures/Augmentation.jpg "Impact of training data augmentation"
![alt text][image11]
**Figure 11: Impact of training data augmentation**

### Impact of deeper architecture
An additional convolutional layer consisting of 32 filters of size 3x3 with stride of 1 and no padding was added to the aforementioned architecture. The number of training samples increased from 34799 to 129196 samples after augmentation. The number of epochs are set to 100, minibatch size set to 128, no dropout, and learning rate set to 0.001. The performance on the validation set is more or less the same with the additional layer.

[image12]: ./figures/Architecture.jpg "Impact of deeper architecture"
![alt text][image12]
**Figure 12: Impact of deeper architecture**

## Final model
Based on the above set of analyses, the final model included normalization, training data augmentation, number of epochs set to 100, minibatch size of 64, learning rate of 0.0005, dropout rate of 0.25 (=0.75 keep probability), and L2 weight regularization. No additional layer was added to the architecture.
The accuracy was 0.948 (maximum accuracy was 0.957) on the validation set, and 0.943 on the test set.
The table below depicts the precision, recall, and f1-score on each of the traffic sign class in the test set.

                                                    precision    recall  f1-score   support

                              Speed limit (20km/h)       0.88      1.00      0.94        60
                              Speed limit (30km/h)       0.89      0.99      0.94       720
                              Speed limit (50km/h)       0.96      0.98      0.97       750
                              Speed limit (60km/h)       0.97      0.93      0.95       450
                              Speed limit (70km/h)       0.99      0.95      0.97       660
                              Speed limit (80km/h)       0.92      0.90      0.91       630
                       End of speed limit (80km/h)       0.99      0.84      0.91       150
                             Speed limit (100km/h)       0.95      0.87      0.91       450
                             Speed limit (120km/h)       0.86      1.00      0.92       450
                                        No passing       0.96      0.99      0.97       480
      No passing for vehicles over 3.5 metric tons       0.96      1.00      0.98       660
             Right-of-way at the next intersection       0.92      0.91      0.92       420
                                     Priority road       0.98      0.98      0.98       690
                                             Yield       0.99      1.00      0.99       720
                                              Stop       1.00      0.95      0.97       270
                                       No vehicles       0.97      0.97      0.97       210
          Vehicles over 3.5 metric tons prohibited       1.00      0.99      1.00       150
                                          No entry       1.00      0.95      0.97       360
                                   General caution       0.98      0.83      0.90       390
                       Dangerous curve to the left       0.98      1.00      0.99        60
                      Dangerous curve to the right       0.77      0.93      0.84        90
                                      Double curve       0.98      0.64      0.78        90
                                        Bumpy road       0.96      0.90      0.93       120
                                     Slippery road       0.92      0.97      0.94       150
                         Road narrows on the right       0.86      0.70      0.77        90
                                         Road work       0.92      0.95      0.93       480
                                   Traffic signals       0.83      0.85      0.84       180
                                       Pedestrians       0.58      0.50      0.54        60
                                 Children crossing       0.88      0.97      0.93       150
                                 Bicycles crossing       0.86      0.99      0.92        90
                                Beware of ice/snow       0.80      0.87      0.83       150
                             Wild animals crossing       0.97      0.98      0.97       270
               End of all speed and passing limits       0.85      1.00      0.92        60
                                  Turn right ahead       0.97      0.94      0.95       210
                                   Turn left ahead       0.92      0.99      0.95       120
                                        Ahead only       0.95      0.97      0.96       390
                              Go straight or right       0.97      0.94      0.96       120
                               Go straight or left       1.00      0.95      0.97        60
                                        Keep right       0.95      0.95      0.95       690
                                         Keep left       0.98      0.97      0.97        90
                              Roundabout mandatory       0.86      0.80      0.83        90
                                 End of no passing       1.00      0.60      0.75        60
     End of no passing by vehicles over 3.5 metric       0.99      0.97      0.98        90

                                       avg / total       0.94      0.94      0.94     12630

## Test the model on new images

Five images of german traffic signs were downloaded from the web and subjected to the same preprocessing steps of normalization. The normalized images were run through the model. All the images were correctly classified with an overall accuracy of 1.0. The figures below illustrate the true image and the corresponding top 5 softmax probabilities depicted in the form of a bar chart. Only the first image consisting of a stop sign, though correctly classified, had a softmax probability of 0.32. This may be due to the presence of another sign below it which is partially occluded.

[image13]: ./figures/New_data_result0.jpg "Stop sign"
![alt text][image13]
**Figure 13: Stop sign**

[image14]: ./figures/New_data_result1.jpg "Speed limit 30"
![alt text][image14]
**Figure 14: Speed limit 30**

[image15]: ./figures/New_data_result2.jpg "Yield"
![alt text][image15]
**Figure 15: Yield**

[image16]: ./figures/New_data_result3.jpg "Children crossing"
![alt text][image16]
**Figure 16: Children crossing**

[image17]: ./figures/New_data_result4.jpg "Roadwork"
![alt text][image17]
**Figure 17: Roadwork**

## Visualize the model states on a new image

While neural networks can be a great learning device they are often referred to as a black box. We can understand what the weights of a neural network look like better by plotting their feature maps. After successfully training your neural network you can see what it's feature maps look like by plotting the output of the network's weight layers in response to a test stimuli image. From these plotted feature maps, it's possible to see what characteristics of an image the network finds interesting. For a sign, maybe the inner network feature maps react with high activation to the sign's boundary outline or to the contrast in the sign's painted symbol.
The figures below illustrate the raw image and the corresponding feature maps after convolutional layer 1, max pooling layer 1, convolutional layer 2, and max pooling layer 2.

[image18]: ./figures/Speed_limit_30.jpg "Speed limit 30"
![alt text][image18]
**Figure 18: Speed limit 30**

[image19]: ./figures/Visualization_conv1.jpg "Visualization of conv layer 1"
![alt text][image19]
**Figure 19: Visualization of conv layer 1**

[image20]: ./figures/Visualization_pool1.jpg "Visualization of pooling layer 1"
![alt text][image20]
**Figure 20: Visualization of pooling layer 1**

[image21]: ./figures/Visualization_conv2.jpg "Visualization of conv layer 2"
![alt text][image21]
**Figure 21: Visualization of conv layer 2**

[image22]: ./figures/Visualization_pool2.jpg "Visualization of pooling layer 2"
![alt text][image22]
**Figure 22: Visualization of pooling layer 2**


## Shortcomings of the proposed approach

The training and validation data are highly imbalanced in terms of number of samples in each class. The data augmentation did not account for affine transformation of the images that are typically seen in traffic sign images captured from different angles. Only one model architecture based on LeNet was used to build the model. 

## Possible improvements

The data augmentation method could be extended to account for affine transformation, left to right transformation. Additional samples could be generated during data augmentation to make number of samples in each class balanced. Additional architectures based on VGG or variants of Resnet could be used.

### Acknowledgments

I would like to thank Udacity for giving me this opportunity to work on an awesome project. 
