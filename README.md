# Traffic sign recognition

Recognizing different traffic signs with high accuracy is a must-have requirement for any self driving car. The traffic sign recognition is a classification problem that could be solved with traditional computer vision approach involving building a pipeline consisting of representing different traffic signs with hand-crafted features and classifying them using traditional classifier like support vector machine (SVM), random forest (RF), etc. Generating hand-crafted features is a challenging task due to wide variability in the traffic sign data. Recently, the subfield of machine learning called deep learning have shown highly promising performance in wide variety of applications due to recent advancements in computational power and scientific methods. This has made possible the implementation of computationally demanding neural networks with deep architectures which have the property of universal function approximation. In other words, neural networks can be used to fit any nonlinear function with enough number of layers, each represented with lots of parameters. The parameters can later be pruned with regularization methods such as dropout, L2 regularization, etc. to enhance the generalization ability of the network architecture to new sets of images. 
In this study, we apply the deep learning architecture based on the combination of convolutional neural networks (CNNs) and fully connected layers for modeling traffic sign images. The data used are [German traffic sign](http://benchmark.ini.rub.de/?section=gtsrb&subsection=dataset) images and the base neural network architecture is [LeNet](http://yann.lecun.com/exdb/publis/pdf/lecun-01a.pdf). The processing steps include data exploration  and visualization for understanding the data, normalization for preprocessing the data, training the preprocessed data using deep learning architecture that includes convolutional, max pooling, fully connected layers interspersed with nonlinearity based on rectified linear units (ReLUs) and regularization based on dropout and L2 regularization to build the model. 
The impact of each of these processing steps is illustrated during the process of hyperparameter selection for parameters such as learning rate, dropout rate, weight regularization, batch size, and number of epochs. Adam optimizer is used for optimizing the network parameters. The analysis uses LeNet architecture as the basic architecture, and the impact of adding an additional convolutional layer is also presented. To make the model more robust to influence of varieties of environmental sources of noise such as abrupt weather conditions, changes in lightning conditions, etc. data augmentation methods are applied to the training data before building the final model. The performance is quantified using metrics based on overall accuracy, and recall, precision, and f1-score for each of the classes. The final model is also applied on an independent set of 5 images downloaded from the web.

## Dependencies

This project requires:

* [CarND Term1 Starter Kit](https://github.com/udacity/CarND-Term1-Starter-Kit)

The lab environment can be created with CarND Term1 Starter Kit. Click [here](https://github.com/udacity/CarND-Term1-Starter-Kit/blob/master/README.md) for the details.

## Dataset 

The original Udacity project instructions can be found [here](https://github.com/udacity/CarND-Traffic-Sign-Classifier-Project).

[Download the dataset](https://d17h27t6h515a5.cloudfront.net/topher/2016/November/581faac4_traffic-signs-data/traffic-signs-data.zip). This is a pickled dataset in which the images have been to size 32x32.


## Detailed Writeup

Detailed report can be found in [_Traffic_Sign_Classifier_writeup.md_](Traffic_Sign_Classifier_writeup.md). 

### Acknowledgments

I would like to thank Udacity for giving me this opportunity to work on an awesome project. 
